<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Virtual Doctor-Patient</title>
	<!-- Linking Bootstrap css-->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<!-- Linking Custom css-->
	<link rel="stylesheet" href="assets/css/style.css">
	<!-- Custom fonts for this template -->
      <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
      <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
      <link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel='stylesheet' type='text/css'>
      <link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic" rel='stylesheet' type='text/css'>
      <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
</head>
<body>
	 <!-- Navigation -->
      <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
         <div class="container">
            <a class="navbar-brand js-scroll-trigger" href="#page-top">Virtual Doctor Patient</a>
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            Menu
            <i class="fa fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
               <ul class="navbar-nav text-uppercase ml-auto">
                  <li class="nav-item">
                     <a class="nav-link js-scroll-trigger" href="#">Home</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link js-scroll-trigger" href="#services">Services</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link js-scroll-trigger" href="#team">Team</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
                  </li>
                  <li class="nav-item">

                    <?php 

                        if(isset($_SESSION['username']))
                        {
                          echo '<a class="nav-link js-scroll-trigger" href="logout.php">Logout</a>';
                        }
                        else
                        {
                          echo '<a class="nav-link js-scroll-trigger" href="#" data-toggle="modal" data-target="#LoginModal">Login</a>';
                        }
                    ?>
                     
                  </li>
               </ul>
            </div>
         </div>
      </nav>
      <!-- Header -->
      <header class="masthead">
         <div class="container">
         <div class="intro-text">
         <div class="intro-lead-in">Welcome To Virtual Doctor-Patient</div>
         <div class="intro-heading text-uppercase">We are here to Treat You</div>
         <button type="button" class="btn btn-primary btn-lg text-uppercase" data-toggle="modal" data-target="#RegisterModal">Register Now</button>
         </button>
         <!-- The Modal for registration -->
         <div class="modal" id="RegisterModal">
         <div class="modal-dialog">
         <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
               <h4 class="modal-title">Register Now</h4>
               <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
               <form action="index.php" method="post" autocomplete="off" id="registration" onsubmit="validateForm();">
                  <label for="fullname" style="color:#211f1f;">Full Name</label>
                  <input type="text" name="fullname" placeholder="Full Name" />
                  <br/><br/>
                  <label for="email" style="color:#211f1f;">Email Address</label>
                  <input type="email" name="email" placeholder="Email Address" />
                  <br/><br/>
                  <label for="username" style="color:#211f1f;">UserName</label>
                  <input type="text" name="username" placeholder="UserName" />
                  <br/><br/>
                  <label for="password" style="color:#211f1f;">Password</label>
                  <input type="password" name="password" placeholder="Password" />
                  <br/><br/>
                  <label for="confirm_password" style="color:#211f1f;">Confirm Password</label>
                  <input type="password" name="confirm_password" placeholder="Confirm Password" />
                  <br/><br/>
                  <select name="user_type">
                     <option value="doctor">Doctor</option>
                     <option value="patient">Patient</option>
                  </select>
                  <button type="submit" name="register" class="btn btn-primary">Register</button>
                  <button type="button" name="login" class="btn btn-primary" data-toggle="modal" data-target="#LoginModal">Login</button>   
                  </form>     
                  <!-- Modal footer -->
                  <div class="modal-footer">
                     <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                  </div>
            </div>
         </div>
       </div>
     </div>
   <!-- The Modal for Login -->
         <div class="modal" id="LoginModal">
         <div class="modal-dialog">
         <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
               <h4 class="modal-title">Login Now</h4>
               <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
               <form action="login.php" method="post" autocomplete="off">
                  <label for="username" style="color:#211f1f;">UserName</label>
                  <input type="text" name="username" placeholder="UserName" />
                  <br/><br/>
                  <label for="password" style="color:#211f1f;">Password</label>
                  <input type="password" name="password" placeholder="Password" />
                  <br/><br/>
                  <select name="user_type">
                     <option value="doctor">Doctor</option>
                     <option value="patient">Patient</option>
                  </select>
                  <input type="submit" name="login" class="btn btn-primary" value="login" id="login">
                  </form>     
                  <!-- Modal footer -->
                  <div class="modal-footer">
                     <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                  </div>
            </div>
         </div>
       </div>
     </div>
      </header>
      <!-- Services -->
      <section id="services">
      <div class="container">
      <div class="row">
      <div class="col-lg-12 text-center">
      <h2 class="section-heading text-uppercase">Services</h2>
      <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
      </div>
      </div>
      <div class="row text-center">
      <div class="col-md-4">
      <span class="fa-stack fa-4x">
      <i class="fa fa-circle fa-stack-2x text-primary"></i>
      <i class="fa fa-user-md fa-stack-1x fa-inverse"></i>
      </span>
      <h4 class="service-heading">Online Patient Monitoring</h4>
      <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p>
      </div>
      <div class="col-md-4">
      <span class="fa-stack fa-4x">
      <i class="fa fa-circle fa-stack-2x text-primary"></i>
      <i class="fa fa-camera fa-stack-1x fa-inverse"></i>
      </span>
      <h4 class="service-heading">Live Video advice</h4>
      <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p>
      </div>
      <div class="col-md-4">
      <span class="fa-stack fa-4x">
      <i class="fa fa-circle fa-stack-2x text-primary"></i>
      <i class="fa fa-heart fa-stack-1x fa-inverse"></i>
      </span>
      <h4 class="service-heading">Health's Tips</h4>
      <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p>
      </div>
      </div>
      </div>
      </section>
      <!-- Team -->
      <section class="bg-light" id="team">
      <div class="container">
      <div class="row">
      <div class="col-lg-12 text-center">
      <h2 class="section-heading text-uppercase">Our Best Doctors</h2>
      <h3 class="section-subheading text-muted">Here is our Best Doctors</h3>
      </div>
      </div>
      <div class="row">
      <div class="col-sm-4">
      <div class="team-member">
      <img class="mx-auto rounded-circle" src="assets/img/team/doctor-team.jpg" alt="">
      <h4>Kay Garland</h4>
      <p class="text-muted">Doctor</p>
      <ul class="list-inline social-buttons">
      <li class="list-inline-item">
      <a href="#">
      <i class="fa fa-twitter"></i>
      </a>
      </li>
      <li class="list-inline-item">
      <a href="#">
      <i class="fa fa-facebook"></i>
      </a>
      </li>
      <li class="list-inline-item">
      <a href="#">
      <i class="fa fa-linkedin"></i>
      </a>
      </li>
      </ul>
      </div>
      </div>
      <div class="col-sm-4">
      <div class="team-member">
      <img class="mx-auto rounded-circle" src="assets/img/team/doctor-team.jpg" alt="">
      <h4>Larry Parker</h4>
      <p class="text-muted">Doctor</p>
      <ul class="list-inline social-buttons">
      <li class="list-inline-item">
      <a href="#">
      <i class="fa fa-twitter"></i>
      </a>
      </li>
      <li class="list-inline-item">
      <a href="#">
      <i class="fa fa-facebook"></i>
      </a>
      </li>
      <li class="list-inline-item">
      <a href="#">
      <i class="fa fa-linkedin"></i>
      </a>
      </li>
      </ul>
      </div>
      </div>
      <div class="col-sm-4">
      <div class="team-member">
      <img class="mx-auto rounded-circle" src="assets/img/team/doctor-team.jpg" alt="">
      <h4>Diana Pertersen</h4>
      <p class="text-muted">Doctor</p>
      <ul class="list-inline social-buttons">
      <li class="list-inline-item">
      <a href="#">
      <i class="fa fa-twitter"></i>
      </a>
      </li>
      <li class="list-inline-item">
      <a href="#">
      <i class="fa fa-facebook"></i>
      </a>
      </li>
      <li class="list-inline-item">
      <a href="#">
      <i class="fa fa-linkedin"></i>
      </a>
      </li>
      </ul>
      </div>
      </div>
      </div>
      <div class="row">
      <div class="col-lg-8 mx-auto text-center">
      <p class="large text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut eaque, laboriosam veritatis, quos non quis ad perspiciatis, totam corporis ea, alias ut unde.</p>
      </div>
      </div>
      </div>
      </section>
      <!-- Contact -->
      <section id="contact">
        <div class="container">
          <div class="row">
            <div class="col-lg-12 text-center">
              <h2 class="section-heading text-uppercase">Contact Us</h2>
              <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12">
              <form id="contactForm" name="sentMessage" novalidate="novalidate">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <input class="form-control" id="name" type="text" placeholder="Your Name *" required="required" data-validation-required-message="Please enter your name.">
                      <p class="help-block text-danger"></p>
                    </div>
                    <div class="form-group">
                      <input class="form-control" id="email" type="email" placeholder="Your Email *" required="required" data-validation-required-message="Please enter your email address.">
                      <p class="help-block text-danger"></p>
                      </div>
                <div class="form-group">
      <input class="form-control" id="phone" type="tel" placeholder="Your Phone *" required="required" data-validation-required-message="Please enter your phone number.">
      <p class="help-block text-danger"></p>
      </div>
      </div>
      <div class="col-md-6">
      <div class="form-group">
      <textarea class="form-control" id="message" placeholder="Your Message *" required="required" data-validation-required-message="Please enter a message."></textarea>
      <p class="help-block text-danger"></p>
      </div>
      </div>
      <div class="clearfix"></div>
      <div class="col-lg-12 text-center">
      <div id="success"></div>
      <button id="sendMessageButton" class="btn btn-primary btn-xl text-uppercase" type="submit">Send Message</button>
      </div>
      </div>
      </form>
      </div>
      </div>
      </div>
      </section>

	<!-- Linking Bootstrap js and jquery-->
     <script src="assets/js/jquery.min.js"></script>
     <script src="assets/js/bootstrap.min.js"></script>
     <!-- Plugin JavaScript -->
     <script src="assets/jquery-easing/jquery.easing.min.js"></script>
     <!-- Contact form JavaScript -->
     <script src="js/jqBootstrapValidation.js"></script>
     <script src="assets/js/contact_me.js"></script>
     <!-- Custom scripts for this template -->
     <script src="assets/js/main.js"></script>

</body>
</html>